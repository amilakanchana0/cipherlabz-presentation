import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { AddUserComponent } from '../add-user/add-user.component';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-all-users',
  templateUrl: './view-all-users.component.html',
  styleUrls: ['./view-all-users.component.css']
})
export class ViewAllUsersComponent implements OnInit {

  users:User[]=[]
  constructor(private userService:UserService,public dialog: MatDialog, private toastr:ToastrService, private router:Router) { }
  displayedColumns: string[] = ['userName', 'name', 'telephoneNo', 'userType', 'action'];
  dataSource = new MatTableDataSource(this.users);

  ngOnInit() {
    this.userService.getUsers().subscribe(users=>{
      this.users = users;
      this.dataSource.data = this.users
    })
  }

  updateUser(user){
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '900px', data: user
    });
  }

  async deleteUser(user){
    this.userService.deleteUser(user);
    this.toastr.warning('User Deleted')
    await delay(300);
    this.ngOnInit();
   
  }

  openAddUser(){
  this.router.navigate(['adduser']);
  }

}
async function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}