import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { error } from '@angular/compiler/src/util';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class UserService {

  HOME_URL='http://localhost:3000/';
  users: Observable<User[]>;

  constructor(private http: HttpClient) { }

  addUser(user: User) {
    
    const url = this.HOME_URL + 'users';
    return this.http.post(url, user).subscribe(res => {
      console.log(res);
      return res;
    });

  }

  getUsers(): Observable<User[]> {
    this.users = this.http.get<User[]>(this.HOME_URL + 'users');
    return this.users;

  }

  updateUser(user) {
    const url = this.HOME_URL + 'users/' + user.id;
    return this.http.put(url, user).subscribe(res => {
      console.log(res);
      return res;
    });
  }
  deleteUser(user) {
    const url = this.HOME_URL + 'users/' + user.id;
    return this.http.delete(url).subscribe(res => {
      return res;
    });
  }
}
