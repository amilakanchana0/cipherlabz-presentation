import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {AddUserComponent} from 'src/app/add-user/add-user.component';
import {HeaderComponent} from 'src/app/header/header.component';
import {SideNavComponent} from 'src/app/side-nav/side-nav.component';
import { ViewAllUsersComponent} from 'src/app/view-all-users/view-all-users.component';
import {MatDatepickerModule,MatFormFieldModule,MatInputModule,MatSelectModule,MatCardModule,MatTableModule, MatDialogModule, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { FormsModule } from '@angular/forms';
import {  HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,AddUserComponent,HeaderComponent,SideNavComponent,ViewAllUsersComponent
  ],
  imports: [
    BrowserModule,MatButtonModule,MatIconModule,MatDatepickerModule,MatFormFieldModule,MatInputModule,MatSelectModule,MatCardModule,MatTableModule,FormsModule,
    AppRoutingModule,MatDialogModule,
    BrowserAnimationsModule,HttpClientModule ,ToastrModule.forRoot()
  ],
  entryComponents:[AddUserComponent],

  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
