import { Component, OnInit, Inject } from "@angular/core";
import { User } from "../models/user.model";
import { ToastrService } from "ngx-toastr";
import { UserService } from "../services/user.service";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.css"]
})
export class AddUserComponent implements OnInit {
  user: User = {
    id: "",
    address: "",
    fullName: "",
    password: "",
    telephoneNo: "",
    userName: "",
    userType: ""
  };
  action: string = "Add User";
  actionType = 0;

  types: any[] = [
    { value: "Admin", viewValue: "Admin" },
    { value: "User", viewValue: "User" },
    { value: "Technician", viewValue: "Technician" },
    { value: "Doctor", viewValue: "Doctor" }
  ];

  constructor(
    private userService: UserService,
    public dialogRef: MatDialogRef<AddUserComponent>,
    private _router: Router,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) private data: User
  ) {}

  ngOnInit() {
    if (this.data.userName != null) {
      this.action = "Update User";
      this.actionType = 1;
      this.user = this.data;
      console.log(this.data);
    }
  }
  onNoClick(): void {
    if (this.actionType == 0) {
      this._router.navigate(["allusers"]);
    } else {
      this.dialogRef.close();
    }
  }

  async addUser() {
    if (this.actionType == 0) {
      this.userService.addUser(this.user);
      this.toastr.success("User Added Successfully");
      await delay(300);
      this._router.navigate(["allusers"]);
    } else {
      this.userService.updateUser(this.user);
      this.toastr.info("User Updated Successfully");
      await delay(300);
      this.dialogRef.close();
    }
  }
}
async function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
