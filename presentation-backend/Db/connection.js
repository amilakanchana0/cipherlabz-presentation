const Sequelize = require("sequelize");
module.exports.dbCon = new Sequelize("presentation", "user", "user", {
  host: "localhost",
  dialect: "mysql",
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});
