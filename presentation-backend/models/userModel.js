;const Sequelize = require('sequelize');
const db = require('../Db/connection')

const User = db.dbCon.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true ,
        autoIncrement: true,
        
    },
    userName: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    fullName: {
        type: Sequelize.STRING
    },
    telephoneNo: {
        type: Sequelize.INTEGER
    },
    address: {
        type: Sequelize.STRING
    },
    userType: {
        type: Sequelize.STRING
    }
   
},
    {

        timestamps: false
    }
);
module.exports = User;