var express = require('express');
var router = express.Router();
var userService = require('../services/userService')
//get all users
router.get('/', function(req, res, next) {
    userService.getAllUsers().then(users=>{
      console.log(users)
      res.send(users);
    });

});

//get single user by id
router.get('/:id', function(req, res, next) {
  res.send(req.params.id);
});

//add new user
router.post('/', function(req, res, next) {
  userService.createUser(req.body).then(()=>{
    res.send(req.body);
  })
  
});

//update a user
router.put('/:id', function(req, res, next) {
  userService.updateUser(req.body, req.params.id).then(()=>{
    res.send(req.body);
  })
});

//delete a user
router.delete('/:id', function(req, res, next) {
 userService.deleteUser(req.params.id).then(res=>{
   res.send(res)
 })
});

module.exports = router;
