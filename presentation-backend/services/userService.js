
const User = require('../models/userModel')

module.exports.createUser = function (newUser) {
    let { userName, password, fullName, telephoneNo, address, userType } = newUser;
    return User.create({
        userName, password, fullName, telephoneNo, address, userType
    })
        .then(user => (console.log(user)))
        .catch(err => console.log(err))
}

module.exports.updateUser = function (user, id) {
    let { userName, password, fullName, telephoneNo, address, userType } = user;
    User.update({ userName, password, fullName, telephoneNo, address, userType },
        { where: { id: id } }
    )
        .then(user => (console.log(user)))
        .catch(err => console.log(err))
}

module.exports.deleteUser = function (id) {
    User.destroy({
        where: { id: id }
    })
        .then(user => (console.log(user)))
        .catch(err => console.log(err))
}

module.exports.getAllUsers = function () {
    return User.findAll({ raw: true }).then(users => {
        return users
    })
}

module.exports.getUserById = function (id) {
    User.findById(id).then(user => {
        console.log(user);
    });
}

